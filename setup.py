from requests import get, post
from json import loads
from os import path
from sqlalchemy import create_engine

### IMPORTANT!!! EDIT THIS BEFORE LAUNCHING!!! ###
with open("./config.json", "r") as f:
    cfg = loads(f.read())
### IMPORTANT!!! EDIT THIS BEFORE LAUNCHING!!! ###

def appReg():
    appRegUrl = cfg["base_url"] + "/api/v1/apps"
    appRegData = {"client_name": "yeetdelete", "redirect_uris": cfg["redirect_uri"], "scopes": cfg["scopes"]}
    try:
        appRegReq = post(url=appRegUrl, data=appRegData)
        with open("./appdetails.secret", "w") as f:
            f.write(appRegReq.text)
        return(appRegReq.text)
    except:
        print("Could not register app. Double check your base url in the config.json, wait a little bit and try again.")
        exit()

def getAuthCode(appDetails):
    getAuthCodeUrl = cfg["base_url"] + "/oauth/authorize"
    print("Please follow this link, log in, and paste the token into the prompt. Sorry.\n")
    print(getAuthCodeUrl + "?response_type=code&client_id=" + appDetails["client_id"] + "&redirect_uri=" + cfg["redirect_uri"] + "&scope=" + cfg["scopesGET"] + "\n")
        

def getToken(appDetails):
    getTokenUrl = cfg["base_url"] + "/oauth/token"
    userCode = input("Token code: ")
    getTokenData = {"client_id": appDetails["client_id"], "client_secret": appDetails["client_secret"], "redirect_uri": cfg["redirect_uri"], "scope": cfg["scopes"], "code": userCode, "grant_type": cfg["grant_type"]}
    try:
        getTokenReq = post(url=getTokenUrl, data=getTokenData)
        with open("./token.secret", "w") as f:
            f.write(getTokenReq.text)
    except:
        print("Could not get token. Your server might have skipped a beat. Wait a little bit and try again.")
        exit()

### This is where the magic happens ###

if not path.exists("./token.secret"):

    if not path.exists("./appdetails.secret"):
        print("No app detected. Registering app...")
        appRegOutput = loads(appReg())
    else:
        with open("./appdetails.secret", "r") as f:
            appRegOutput = loads(f.read())

    print("Getting token...")
    getAuthCode(appRegOutput)
    getToken(appRegOutput)
    print("Making DB...")
    try:
        dbLink = cfg["mysqlurl"]
        engine = create_engine(dbLink)
        engine.execute("CREATE DATABASE yeetdelete")
        print("Setup is complete! Feel free to run run.py now.")
    except:
        print("Setup is complete! Feel free to run run.py now.")
else:
    try:
        dbLink = cfg["mysqlurl"]
        engine = create_engine(dbLink)
        engine.execute("CREATE DATABASE yeetdelete")
    except:
        print("Everything is already set up! Feel free to run run.py now.")