# yeetdelete

## About

yeetdelete is a Pleroma post deleting script, inspired by [forget](https://github.com/codl/forget). By default, yeetdelete will delete posts older than 30 days. This variable is editable in the config.

This is my first proper venture into Python. Suggestions and improvements are always welcome. 

## Prerequisites

* MySQL 8+ (tested on 8.0.20, might work on older versions)
* Python 3+ (tested on 3.7, might work on older versions)
* Pip
* MySQL C API client library. On Ubuntu/Debian this is `$ apt install libmysqlclient-dev`

## Install dependencies

```
$ pip3 install -r requirements.txt
```

## Setup

1. Check config.json, edit the base (instance) URL and MySQL connection URL. Optionally, enable the fireworks, they're fun to watch.
2. Run `setup.py`.
3. If you have a lot of posts, it is recommended to run `run.py` at least once before deploying. Enjoy the fireworks if you enabled them!

## Usage

Works with cron. Here's my crontab line: `0 0 * * * cd ~/yeetdelete && python3 run.py`.

Alternatively, just run `run.py` once (ideally twice) if you want to delete all posts older than the set threshold as a one-time operation.

## Q&A

Q: How do I enable fireworks?<br>
A: Set `enableFireworks` in the config to "True" or "true".

Q: Your script skipped a post!<br>
A: This script is designed with eventual consistency in mind. Run it again.

Q: Why MySQL?<br>
A: Because I'm a masochist.

Q: I don't want to install MySQL...<br>
A: If you're fine with Docker, follow these three steps... (you won't believe number 2!)

1. `$ docker run --detach --name=yeetsql --env="MYSQL_ROOT_PASSWORD=yeetdelete" mysql`
2. `$ docker inspect yeetsql`
3. Find the "IPAddress" parameter. If installed Docker just for this, it's probably `172.17.0.2`. That's the URL, shove that into `config.json`

Q: Something broke!<br>
A: DM [zonk](https://neckbeard.xyz/nosleep) with the output of the script.

## Roadmap

- [x] OAuth2 Auth
- [x] Initial DB setup
- [x] Save all posts to DB
- [x] Rescrape posts when rerun
- [x] Proper try/except handling
- [x] Post deletion from DB and instance
- [x] Tidying up the code
- [x] Rewrite DB setup without sqlalchemy_utils
- [x] Test in clean environment
- [x] Create requirements.txt
- [x] Finish README
- [x] Add pointless easter eggs

## Thanks

- [codl](https://chitter.xyz/@codl) for creating [forget](https://github.com/codl/forget)!
- [0x00](https://social.panthermodern.net/@0x00) for the idea to create "forget but for Pleroma"!