from requests import get, post, delete
from json import loads
from os import path, remove
from re import findall
from datetime import datetime, timedelta
from sqlalchemy import create_engine, Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

### You should have edited this by now ###
with open("./config.json", "r") as f:
    cfg = loads(f.read())
### You should have edited this by now ###

if not path.exists("./token.secret"):
    print("token.secret is missing. Please rerun setup.py")
    exit()
else:
    with open("./token.secret", "r") as f:
        token = loads(f.read())

def checkToken():
    checkTokenUrl = cfg["base_url"] + "/api/v1/accounts/verify_credentials"
    checkTokenBearer = "Bearer " + token["access_token"]
    checkTokenHeader = {"Authorization": checkTokenBearer}
    try:
        checkTokenReq = get(url=checkTokenUrl, headers=checkTokenHeader)
    except:
        print("Your server isn't responding. Please wait a bit and try again")
        exit()
    if checkTokenReq.status_code == 200:
        checkTokenReqJson = loads(checkTokenReq.text)
        accountID = checkTokenReqJson["id"]
        return accountID
    elif checkTokenReq.status_code == 401:
        print("Token is invalid/incorrect. Delete token.secret and run setup.py again")
        exit()

def dateConverter(date):
    dater = datetime.strptime(date[:-5], "%Y-%m-%dT%H:%M:%S") #No, I dont give a *shit* about the microseconds, bite me!
    return(dater.strftime("%Y-%m-%d %H:%M:%S"))

def postParser(posts):
    if cfg["enableFireworks"] == "True" or cfg["enableFireworks"] == "true": #Yep, I know, not the best code here, with the whole duplication thing. Feel free to submit a pull request if you know how to do it better.
        ids = []
        for post in reversed(posts):
            if post["reblog"] == None:
                date = dateConverter(post["created_at"])
                postId = post["id"]
                session.add(postsToDb(postID=postId, postDate=date))
                ids.append(postId)
            else:
                reblogDate = dateConverter(post["created_at"])
                reblogId = post["reblog"]["id"]
                session.add(reblogsToDb(reblogID=reblogId, reblogDate=reblogDate))
                ids.append(reblogId)
        print(ids)
    else:
        for post in reversed(posts):
            if post["reblog"] == None:
                date = dateConverter(post["created_at"])
                postId = post["id"]
                session.add(postsToDb(postID=postId, postDate=date))
            else:
                reblogDate = dateConverter(post["created_at"])
                reblogId = post["reblog"]["id"]
                session.add(reblogsToDb(reblogID=reblogId, reblogDate=reblogDate))

def pleromaWorkaround():
    lastPost = session.query(postsToDb).order_by(postsToDb.id.desc()).first()
    firstPost = session.query(postsToDb).order_by(postsToDb.id.asc()).first()

    pleromaWorkaroundDel(lastPost)
    pleromaWorkaroundDel(firstPost)

    if session.new or session.dirty or session.deleted:
        session.commit()

def getStatuses(accID, *args):
    if args:
        getStatusesUrl = args[0] #Gigabrain work around tbh
    else:
        getStatusesUrl = cfg["base_url"] + "/api/v1/accounts/" + accID + "/statuses?min_id=0"
    getStatusesBearer = "Bearer " + token["access_token"]
    getStatusesHeader = {"Authorization": getStatusesBearer}
    try:
        getStatusesReq = get(url=getStatusesUrl, headers=getStatusesHeader)
        if getStatusesReq.status_code == 401:
            print("Delete token.secret and rerun setup.py before trying again")
            exit()
        elif getStatusesReq.status_code == 410 or getStatusesReq.status_code == 400:
            print("Your account has been suspended/deleted. I'm sorry about your loss.")
            exit()
        elif getStatusesReq.status_code == 500:
            pleromaWorkaround()
            #I literally have no idea why this happens. This has happened to me on many occasions, and no one knows why it's happening. There are no mentions about this on the server logs. Maybe this will help. If it doesn't, try this 20 times, restart Pleroma and pray.
            print("Ran into weird Pleroma bug. Please wait a bit and try again.")
            exit()
    except:
        print("Your server isn't responding. Please wait a bit and try again")
        exit()
    postParser(loads(getStatusesReq.text))
    if "link" in getStatusesReq.headers:
        nextHeaders = getStatusesReq
        while True:
            nextLink = findall('\<(.*?)\>', nextHeaders.headers["link"])
            try:
                nextHeaders = get(nextLink[1], headers=getStatusesHeader)
            except:
                session.commit()
                print("Your server isn't responding. Please wait a bit and try again")
                exit()
            if not nextHeaders.text == "[]":
                try:
                    postParser(loads(nextHeaders.text))
                except:
                    session.commit()
                    print("Your server isn't responding. Please wait a bit and try again")
                    exit()
            if "link" not in nextHeaders.headers:
                break

def pleromaWorkaroundDel(post):
    delPostUrl = cfg["base_url"] + "/api/v1/statuses/" + post.postID
    delPostBearer = "Bearer " + token["access_token"]
    delPostHeader = {"Authorization": delPostBearer}
    delPostReq = delete(url=delPostUrl, headers=delPostHeader)
    if delPostReq.status_code == 200 or delPostReq.status_code == 404:
        session.delete(post)

def delPosts():
    currentTime = datetime.utcnow()
    postsToDelete = session.query(postsToDb).all()
    for posts in postsToDelete:
        if posts.postID == "[]":
            session.delete(posts)
        else:
            diff = currentTime-posts.postDate
            if diff.days > cfg["threshold"]:
                delPostUrl = cfg["base_url"] + "/api/v1/statuses/" + posts.postID
                delPostBearer = "Bearer " + token["access_token"]
                delPostHeader = {"Authorization": delPostBearer}
                delPostReq = delete(url=delPostUrl, headers=delPostHeader)
                if delPostReq.status_code == 200 or delPostReq.status_code == 404: #Skips a few posts (no idea why), but this is a catch-all clause to make sure everything is gone.
                    if cfg["enableFireworks"] == "True" or cfg["enableFireworks"] == "true":
                        print("Deleted " + str(posts.postID)) 
                    session.delete(posts)

def unReblog():
    currentTimeReblog = datetime.utcnow()
    reblogsToDelete = session.query(reblogsToDb).all()
    for reblogs in reblogsToDelete:
        if reblogs.reblogID == "[]":
            session.delete(reblogs)
        else:
            diff = currentTimeReblog-reblogs.reblogDate
            if diff.days > cfg["threshold"]:
                delPostUrl = cfg["base_url"] + "/api/v1/statuses/" + reblogs.reblogID + "/unreblog"
                delPostBearer = "Bearer " + token["access_token"]
                delPostHeader = {"Authorization": delPostBearer}
                delPostReq = post(url=delPostUrl, headers=delPostHeader)
                if delPostReq.status_code == 200 or delPostReq.status_code == 404: #Skips a few reblogs (no idea why), but this is a catch-all clause to make sure everything is gone.
                    if cfg["enableFireworks"] == "True" or cfg["enableFireworks"] == "true":
                        print("Unrebloged " + str(reblogs.reblogID)) 
                    session.delete(reblogs)

### This is where the magic happens ###

dbLink = cfg["mysqlurl"] + "/yeetdelete"
engine = create_engine(dbLink)
base = declarative_base()
Session = sessionmaker(bind=engine)
session = Session()

class postsToDb(base):
    __tablename__ = 'posts'
    id = Column(Integer, primary_key=True)
    postID = Column(String(128))
    postDate = Column(DateTime)

    def __init__(self, postID, postDate):
        self.postID = postID
        self.postDate = postDate

class reblogsToDb(base):
    __tablename__ = 'reblogs'
    id = Column(Integer, primary_key=True)
    reblogID = Column(String(128))
    reblogDate = Column(DateTime)

    def __init__(self, reblogID, reblogDate):
        self.reblogID = reblogID
        self.reblogDate = reblogDate

base.metadata.create_all(engine)
obj = session.query(postsToDb).order_by(postsToDb.id.desc()).first()

if obj == None:
    ### First time import into db ###
    if cfg["enableFireworks"] == "True" or cfg["enableFireworks"] == "true":
        print("Adding posts for the first time. Sit back and enjoy the fireworks...")
    else:
        print("Adding posts for the first time...")
    accID = checkToken()
    getStatuses(accID)
    if session.new or session.dirty or session.deleted:
        session.commit()
    print("Deleting posts older than " + str(cfg["threshold"]) + " days...")
    delPosts()
    unReblog()
    if session.new or session.dirty or session.deleted:
        session.commit()
else:
    ### Use latest post and climb user's post history from here ###
    print("Updating DB...")
    accID = checkToken()
    getStatusesUrl = cfg["base_url"] + "/api/v1/accounts/" + accID + "/statuses?min_id=" + obj.postID
    getStatuses("yeet", getStatusesUrl)
    if session.new or session.dirty or session.deleted:
        session.commit()
    print("Deleting posts older than " + str(cfg["threshold"]) + " days...")
    delPosts()
    unReblog()
    if session.new or session.dirty or session.deleted:
        session.commit()